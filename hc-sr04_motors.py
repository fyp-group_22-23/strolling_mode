# File: hc-sr04_motors.py
# Project: Actka (COMPS456F)
# Date: 28 Jan 2023
# Programmer: Kevin Li
# Purpose: Use the Ultrasonic Sensor HC-SR04 to determine the movement of the motors

import RPi.GPIO as GPIO
import time
import sys

# Set GPIO pins
TRIG = 16
ECHO = 26
IN1 = 5
IN2 = 6
IN3 = 23
IN4 = 24

# Set time interval
INTERVAL = 1

GPIO.setmode(GPIO.BCM)

# Set GPIO pins as output (for motors)
GPIO.setup(IN1,GPIO.OUT)
GPIO.setup(IN2,GPIO.OUT)
GPIO.setup(IN3,GPIO.OUT)
GPIO.setup(IN4,GPIO.OUT)

# Set the rotational frequency by PWM 
motor1_positive=GPIO.PWM(IN1, 10) 
motor1_negative=GPIO.PWM(IN2, 10)
motor2_positive=GPIO.PWM(IN3, 10)
motor2_negative=GPIO.PWM(IN4, 10)

# Set speeds of go-forward and turn left/right
go_rotation_speed = 40
turn_rotation_speed = 65

# An array to store recent THREE distance detected
sameReactions = []

# Initiate all GPIO pins
def distanceInit():
    print('Distance Measurement In Progress')
    GPIO.setup(TRIG,GPIO.OUT)
    GPIO.setup(ECHO,GPIO.IN)

# Start detecting the distance by HC-SR04
def distanceStart():
    GPIO.output(TRIG,True)
    time.sleep(0.00001)
    GPIO.output(TRIG,False)

    while GPIO.input(ECHO) == 0:
        pass
    pulse_start = time.time()

    while GPIO.input(ECHO) == 1:
        pass
    pulse_end = time.time()

    pulse_duration = pulse_end - pulse_start
    distance = pulse_duration * 17150
    distance = round(distance,2)
    # print("Distance:{}cm".format(distance))
    return distance

# Driving forward
def goForward():
    motor1_positive.start(go_rotation_speed)
    motor2_positive.start(go_rotation_speed)
 
# Driving backward  
def goBack():
    motor1_negative.start(go_rotation_speed)
    motor2_negative.start(go_rotation_speed)

# Turning left    
def turnLeft():
    motor1_positive.start(turn_rotation_speed)
    motor2_negative.start(turn_rotation_speed)

# Turing right    
def turnRight():
    motor1_negative.start(turn_rotation_speed)
    motor2_positive.start(turn_rotation_speed)

# Completely stop    
def fullyStop():
    motor1_positive.stop()
    motor1_negative.stop()
    motor2_positive.stop()
    motor2_negative.stop()

# Store and remove items in the sameReactions array
def addDropReactions():
    lengthOfArray = len(sameReactions)
    if lengthOfArray < 3:
        sameReactions.append("same")
    else:
        sameReactions.pop(0)
        sameReactions.append("same")

# Check if the distances in the recentDistances array are similar
def checkSameReactions():
    if len(sameReactions) == 3:
    	sameReactions.clear()
    	return True
    else:
        return False
    
try:
    distanceInit()
    print("Initiated")
    while True:
    	if checkSameReactions() == True:
    	    fullyStop()
    	    time.sleep(1)
    	    turnLeft()
    	    time.sleep(2)
    	else:    
            distance = distanceStart()
            print("Distance:{}cm".format(distance))
            if distance <= 20 and distance > 10:
                fullyStop()
                time.sleep(1)
                turnRight()
                time.sleep(2)
                addDropReactions() 
            if distance <= 10:
                fullyStop()
                time.sleep(1)
                goBack()
                time.sleep(1)
                fullyStop()
                time.sleep(1)
                addDropReactions() 
            else:
                goForward()
                time.sleep(1)
                fullyStop()
                time.sleep(1)
except KeyboardInterrupt:
    GPIO.cleanup()
